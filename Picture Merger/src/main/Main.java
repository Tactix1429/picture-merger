package main;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.imageio.ImageIO;

public class Main {
	static int size = 5;
	static int countAll = 0;
	static float old = -1;
	static String mainPicture;
	static ArrayList<Picture> pictures = new ArrayList<Picture>();

	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);
		System.out.println("Folder with pictures:");
		 String folder = sc.nextLine();
		 System.out.println("Size of replaced chunk:");
		 size = Integer.parseInt(sc.nextLine());
		 System.out.println("Choose main picture:");
		 mainPicture = sc.nextLine();
		listFilesForFolder(new File(folder));
		simplificatePicture(size);
		System.out.println(countAll);
	}

	static void placePicture(int size, BufferedImage output, BufferedImage input, int xOffsetOutput,int yOffsetOutput) {
		for (int xOffset = 0; xOffset < input.getWidth(); xOffset += (input.getWidth() / size)) {
			for (int yOffset = 0; yOffset < input.getHeight(); yOffset += (input.getHeight() / size)) {
				int count = 0;
				long r = 0;
				long g = 0;
				long b = 0;
				for (int x = 0; x < input.getWidth() / size; x++) {
					for (int y = 0; y < input.getHeight() / size; y++) {
						if (x + xOffset < input.getWidth() && y + yOffset < input.getHeight()) {
							count++;
							Color c = new Color(input.getRGB(x + xOffset, y + yOffset));
							r += c.getRed();
							b += c.getBlue();
							g += c.getGreen();
						}
					}
				}
				if (count != 0) {
					r /= count;
					b /= count;
					g /= count;
					//System.out.println(xOffset / (input.getWidth() / size)+" "+yOffsetOutput / (input.getHeight() / size));
					if(xOffsetOutput + (xOffset / (input.getWidth() / size))<output.getWidth()&&yOffsetOutput +yOffset/ (input.getHeight() / size)<output.getHeight())
					{
					output.setRGB(xOffsetOutput + xOffset / (input.getWidth() / size),yOffsetOutput +yOffset/ (input.getHeight() / size),new Color((float) r / 255, (float) g / 255, (float) b / 255).getRGB());
					
					}
				}
				else
				{
					System.out.println("Count was zero!");
				}
			}
		}
	}

	public static void simplificatePicture(int size) throws IOException {
		BufferedImage input = ImageIO.read(new File(mainPicture));
		for (int xOffset = 0; xOffset < input.getWidth(); xOffset += size) {
			for (int yOffset = 0; yOffset < input.getHeight(); yOffset += size) {
				countAll++;
				float percentage = (((float)xOffset*input.getHeight()+yOffset)/((float)input.getHeight()*input.getWidth()))*100;
				if(old!=percentage)
				{
				System.out.println(percentage+" %");
				old = percentage;
				}
				int count = 0;
				long r = 0;
				long g = 0;
				long b = 0;
				for (int x = 0; x < size; x++) {
					for (int y = 0; y < size; y++) {
						if (x + xOffset < input.getWidth() && y + yOffset < input.getHeight()) {
							count++;
							Color c = new Color(input.getRGB(x + xOffset, y + yOffset));
							r += c.getRed();
							b += c.getBlue();
							g += c.getGreen();
						}
					}
				}
				if (count != 0) {
					r /= count;
					b /= count;
					g /= count;
					Picture picture = null;
					double bestDistance = Double.MAX_VALUE;
					double xpic = 2.36460 * r - 0.51515 * g + 0.00520 * b;
					double ypic = -0.89653 * r + 1.42640 * g - 0.01441 * b;
					double zpic = -0.46807 * r + 0.08875 * g + 1.00921 * b;
					for (Picture p : pictures) {
						double xtemp = 2.36460 * p.average.getRed() - 0.51515 * p.average.getGreen()
								+ 0.00520 * p.average.getBlue();
						double ytemp = -0.89653 * p.average.getRed() + 1.42640 * p.average.getGreen()
								- 0.01441 * p.average.getBlue();
						double ztemp = -0.46807 * p.average.getRed() + 0.08875 * p.average.getGreen()
								+ 1.00921 * p.average.getBlue();
						double distance = Math.sqrt(
								Math.pow(xtemp - xpic, 2) + Math.pow(ytemp - ypic, 2) + Math.pow(ztemp - zpic, 2));
						if (distance < bestDistance) {
							bestDistance = distance;
							picture = p;

						}
					}
					if (picture != null) {
						//System.out.println("Best Match:" + bestDistance);
						//System.out.println(xOffset+" "+yOffset);
						placePicture(size, input, ImageIO.read(picture.path), xOffset, yOffset);
					}
				}
			}
		}
		File output = new File("Output_Merger.png");
		ImageIO.write(input, "png", output);

	}

	public static void listFilesForFolder(final File folder) throws IOException {
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				listFilesForFolder(fileEntry);
			} else {
				//System.out.println("Scanning " + fileEntry.getName() + " ");
				pictures.add(new Picture(fileEntry, getAverage(fileEntry.getAbsolutePath())));
			}
		}
	}

	public static Color getAverage(String path) throws IOException {
		long r = 0;
		long g = 0;
		long b = 0;
		BufferedImage br = ImageIO.read(new File(path));
		for (int x = 0; x < br.getWidth(); x++) {
			if (x % (br.getWidth() / 100) == 0) {
				System.out.print(".");
			}
			for (int y = 0; y < br.getHeight(); y++) {
				Color colour = new Color(br.getRGB(x, y));
				r += colour.getRed();
				g += colour.getGreen();
				b += colour.getBlue();
			}
		}
		r /= (br.getHeight() * br.getWidth());
		g /= (br.getHeight() * br.getWidth());
		b /= (br.getHeight() * br.getWidth());
		System.out.println(br.getHeight() * br.getWidth() + " Pixel");
		return new Color((float) r / 255, (float) g / 255, (float) b / 255);

	}

}

class Picture {
	File path;
	Color average;

	public Picture(File path, Color average) {
		this.path = path;
		this.average = average;
	}
}
